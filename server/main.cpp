#include <cstdlib>
#include <string>
#include <list>
#include <iostream>
#include <vector>
#include <Command.h>
#include <Tcp.h>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <sstream>
#include <boost/thread/thread.hpp>
#include <Ftp.h>


#define DEFAULT_SERVER_IP "0.0.0.0"
#define DEFAULT_SERVER_PORT 2121
#define DEFAULT_FILE_NAME "RcvFile"

using namespace std;


list<pair<string,string>> commands = {
        {"run"      ,"run server and listen"},
        {"quit"         ,"quit client"},
};
void serverRun(string ip, uint16_t port ,Logging &logger);

int main(){
    Logging logger("server", true);


    /*commands*/
    vector<string> cmds;
    string userInput;

    /*server threads*/
    vector<boost::thread *> threadVector;


    while (1) {
        cout << "server>";
        getline(cin,userInput);
//        cin >> userInput;
        Command::extractCommand(userInput,cmds);

        if (cmds.at(0).compare("run")==0){


            string ip = DEFAULT_SERVER_IP;
            uint16_t port = DEFAULT_SERVER_PORT;

            if (cmds.size()==3){
                ip=cmds.at(1);
                port = atoi(cmds.at(2).c_str());
            }

            /*create new thread for server */
            boost::thread *thread = new boost::thread(serverRun,ip,port,ref(logger));
            threadVector.push_back(thread);




        }else if (cmds.at(0).compare("quit")==0){
            /*kill all thread*/
            for (boost::thread *thread : threadVector){
//                int threadId = (int) thread->get_id();
                pthread_cancel(thread->native_handle());
//                thread->join();
//                logger.log(LOG_TYPE_INFO, "server thread " + to_string(threadId)+ " is terminated");
            }
            return 0;
        } else{
            logger.log(LOG_TYPE_INFO,"command not found");
        }
    }

    return 0;
}


void serverRun(string ip, uint16_t port ,Logging &logger){

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    Tcp *tcp;
    tcp = new Tcp(ip, port, Tcp::SERVER_MODE);

    logger.log(LOG_TYPE_INFO," server now running on "+ip+":"+to_string(port));

    while (1) {

        tcp->accept();


        /*read command*/
        vector<char> command;
        command.resize(50);
        uint32_t length= command.size();
        tcp->read(&command[0], length);


        Ftp::Command ftpCmd;
        string payload;

        Ftp::extractCmdsFromBytes(command, ftpCmd ,payload);

        switch (ftpCmd){
            case Ftp::FTP_CMD_STOR :{
                logger.log(LOG_TYPE_INFO,"FTP_CMD_STOR " + payload);

                /*read data*/
                vector<char> buffer;
                buffer.resize(10000 << 10);
                int readBytesNum = buffer.size();

                tcp->read(&buffer[0], (uint32_t &) readBytesNum);

/*                auto now = time(nullptr);
                auto tm = *std::localtime(&now);
                stringstream timeSS;
                timeSS << std::put_time(&tm, "-%H-%M-%S");*/

                /*save to file*/
                string fileName = payload;
//                string fileName = DEFAULT_FILE_NAME + timeSS.str();
                ofstream file(fileName, ios::binary);
                if (!file.is_open()) {
                    /*failed to open new file*/
                    logger.log(LOG_TYPE_FAIL, "new file open failed");

                }
                file.write(&buffer[0], readBytesNum);

                file.close();

                /*generate hash*/
                system(string("md5sum " + fileName).c_str());

                break;
            }
            default:{
                logger.log(LOG_TYPE_DEBUG,"ftp command not implement yet");
            }
        }



    }
#pragma clang diagnostic pop
}




