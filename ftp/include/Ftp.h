//
// Created by brian on 3/23/16.
//

#include <cstdint>
#include <string>
#include <Tcp.h>


#ifndef FTP_H
#define FTP_H

#endif //FTPCLIENT_FTP_H

/*

*/
/*user*//*

#define FTP_CMD_USER "USER"
#define FTP_CMD_PASS "PASS"

*/
/*connection*//*

#define FTP_CMD_PORT "PORT"
#define FTP_CMD_QUIT "QUIT"
#define FTP_CMD_MODE_PASSIVE "PASV"
#define FTP_CMD_MODE_BIN "TYPE"

*/
/*dir*//*

#define FTP_CMD_PWD "PWD"
#define FTP_CMD_STOR "STOR"
#define FTP_CMD_DELETE "DELETE"

*/
/*self-defined *//*

#define FTP_CMD_DECODE "DEC"
*/




using namespace std;
class Ftp {
public:
    enum Command{
        FTP_CMD_USER,
        FTP_CMD_PASS,
        FTP_CMD_PORT,
        FTP_CMD_QUIT,
        FTP_CMD_MODE_PASSIVE,
        FTP_CMD_MODE_BIN ,
        FTP_CMD_PWD ,
        FTP_CMD_STOR
    };


    /* common method for FTP*/
    static int cmdToBytes(Command cmd, string payload, vector<char> &returnBytes);
    static int extractCmdsFromBytes(vector<char> &inputBytes, Command &cmd, string &payload);

/*private:
*//*socket variables*//*
    string ip;
    uint16_t port;



public:

    *//*active FTP*//*

    Ftp(string ip, uint16_t port);
    int init();
    int connectToServer();
    int disconnect();
    int uploadFile(string fileName, string fromLocalIP);*//*port is by random*//*


    *//*getter*//*
    int getSocketFD() const {
        return socketFD;
    }

    *//*tools*//*
    int sendCmd(string cmd, string payload);
    int readResponse(string &data, uint16_t length);

    int getErrorCode(int socketFD);
    bool socketIsConnected(int socketFD);


    int checkResponse(string response);*/
};