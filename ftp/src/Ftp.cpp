//
// Created by brian on 2016/5/18.
//


#include <Ftp.h>
#include <sstream>

int Ftp::cmdToBytes(Command cmd, string payload, vector<char> &returnBytes) {
    returnBytes.clear();

    uint8_t commandNum = cmd;
    returnBytes.push_back(commandNum);

    returnBytes.resize(returnBytes.size()+payload.size()+1);
    memcpy(&returnBytes[1],payload.c_str(),payload.length());

    return 0;
}

int Ftp::extractCmdsFromBytes(vector<char> &inputBytes, Command &cmd, string &payload) {
    cmd = (Command) inputBytes[0];

    stringstream ss;
    ss << string(&inputBytes[1]);
    getline(ss,payload);

    return 0;
}



