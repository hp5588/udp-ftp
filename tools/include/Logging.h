//
// Created by brian on 2016/4/16.
//

#ifndef CHATTING_LOGGING_H
#define CHATTING_LOGGING_H

#define DEBUG 1

#define LOG_TYPE_INFO "Info"
#define LOG_TYPE_WARN "Warn"
#define LOG_TYPE_FAIL "Fail"
#define LOG_TYPE_DEBUG "Debug"

#include <cstdint>
#include <string>

using namespace std;

class Logging {
public:
    Logging(string className, bool enable);
    Logging();
    void log(string type, string info);
    bool debugEnable = true;

protected:
    string className;

};


#endif //CHATTING_LOGGING_H
