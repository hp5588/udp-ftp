//
// Created by brian on 2016/4/14.
//

#include <sstream>
#include "../include/Command.h"


int Command::extractCommand(string fullString, vector<string> &commands) {
    stringstream ss (fullString);
    string temp;

    /*clear vector*/
    commands.clear();

    uint8_t count=0;
    while(ss >> temp) {
//        cout << fullString;
        commands.push_back(temp);
/*        if (temp.length()>0){
        } else{
            return count;
        }*/
        count++;
    }
    return count;
}

