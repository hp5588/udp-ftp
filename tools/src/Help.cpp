//
// Created by brian on 2016/4/27.
//

#include <iostream>
#include <iomanip>
#include "../include/Help.h"

Help::Help(list<pair<string, string>> commandList) {
    Help::commandList = commandList;
}

void Help::printHelp() {
    int maxLenghtFirst= 9 ,maxLenghtSec=9;
    for (pair<string,string> item : commandList){
        if (item.first.length()>maxLenghtFirst)
            maxLenghtFirst = item.first.length();

        if (item.second.length()>maxLenghtSec)
            maxLenghtSec = item.second.length();

    }

    cout << "===========Help===========" <<endl;
    cout <<left <<setw(maxLenghtFirst+3) << "COMMAND" << left <<setw(maxLenghtSec+2)<< "FUNCTION" <<endl;
    for (pair<string,string> item :commandList){
        cout << left <<setw(maxLenghtFirst+3) << " "+item.first  << left <<setw(maxLenghtSec+2) << item.second  <<endl;
    }
    cout << "==========================" <<endl;

}



