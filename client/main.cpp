#include <cstdlib>
#include <string>
#include <list>
#include <iostream>
#include <vector>
#include <Command.h>
#include <Tcp.h>
#include <Logging.h>
#include <cstring>
#include <Help.h>
#include <fstream>
#include <Ftp.h>
#include <unistd.h>


#define DEFAULT_SERVER_IP "0.0.0.0"
#define DEFAULT_SERVER_PORT 2121

using namespace std;


list<pair<string,string>> commands = {
        {"connect"      ,"connect to server"},
        {"upload"       ,"upload files to server"},
        {"test"         ,"upload test data to server [DEBUG]"},
        {"help"         ,"print this help"},
        {"quit"         ,"quit client"}
};


int main(){

    Logging logger("client", true);

    /*commands*/
    vector<string> cmds;
    string userInput;

    /*tcp*/
    Tcp *tcp;


    while (1) {
        cout << "client>";
        getline(cin, userInput);
        Command::extractCommand(userInput,cmds);

        if (cmds.at(0).compare("connect")==0){
            string ip = DEFAULT_SERVER_IP;
            uint16_t port = DEFAULT_SERVER_PORT;

            if (cmds.size()==3){
                ip=cmds.at(1);
                port = (uint16_t)atoi(cmds.at(2).c_str());
            }

            tcp = new Tcp(ip,port,Tcp::CLIENT_MODE);

            tcp->connect();



        } else if (cmds.at(0).compare("upload")==0){

            if ((!tcp)||(tcp->getStatus()==Tcp::TCP_DISCONNECT)){
                logger.log(LOG_TYPE_FAIL,"can't upload file without connection");
            }

            string fileName;
            if (cmds.size() == 2){
                fileName = cmds.at(1);
            }


            /*send ftp STOR command*/
            vector<char> commandBytes;
            Ftp::cmdToBytes(Ftp::FTP_CMD_STOR,fileName,commandBytes);

            /*send ftp command through FTP command*/
            tcp->write(&commandBytes[0],commandBytes.size());

            /*wait*/
            usleep(100000);

            /*open a file*/
            ifstream file(fileName,ios::binary|ios::ate);


            if (!file.is_open()){
                 /*open fail*/
                logger.log(LOG_TYPE_FAIL,"can't open the file");
                continue;
            }


            /*get the length of file*/
            int fileLenght = file.tellg();
            /*set pointer to head of file*/
            file.seekg(0);



//            while (!file.eof()){
                /*new clean buffer*/
//            char buffer[fileLenght];
            vector<char> buffer;
            buffer.resize(fileLenght);
            memset(&buffer[0],0,buffer.size());

            /*read data from file*/
            file.read(&buffer[0],buffer.size());

            /*write to client*/
            tcp->write(&buffer[0],file.gcount());

            //            }

            /*close file*/
            file.close();

            /*print hash of the file after successfully sent*/
            system(string ("md5sum "+ fileName).c_str());



        }else if (cmds.at(0).compare("help")==0){
            Help help(commands);
            help.printHelp();

        }else if (cmds.at(0).compare("test")==0){
            if ((!tcp)||(tcp->getStatus()==Tcp::TCP_DISCONNECT)){
                logger.log(LOG_TYPE_FAIL,"can't upload file without connection");
            }

            uint16_t blockCount = 1000;
            vector<char> testData(blockCount<<10);
            for (int blockIndex = 0; blockIndex < blockCount; ++blockIndex) {
                memset(&testData[blockIndex<<10],blockIndex,1<<10);
            }


            tcp->write(&testData[0],testData.size());

        }else if (cmds.at(0).compare("quit")==0){
            return 0;
        }
    }

    return 0;
}