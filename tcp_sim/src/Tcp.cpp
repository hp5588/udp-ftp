//
// Created by brian on 2016/5/7.
//

#include <Tcp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <cstring>
#include <thread>
#include <boost/thread/thread.hpp>


Tcp::Tcp(string ip, uint16_t port, Tcp::Mode mode) {
    Tcp::ip=ip;
    Tcp::port = port;
    Tcp::opMode = mode;

    if (mode == CLIENT_MODE){
        peerIp = ip;
        peerPort = port;
    }

    Tcp::init(mode);
}

bool Tcp::write(char *data, uint32_t length) {


    char *const headPointer = data,
            *readerPointer = headPointer;


    vector<pair<Packet::TcpPacket,bool>> packetArray;

    if (length%PIECE_SIZE>0){
        packetArray.resize(length/PIECE_SIZE+1);
    } else{
        packetArray.resize(length/PIECE_SIZE);
    }


    /*tear data into pieces*/
    for (int i = 0; i < packetArray.size(); ++i) {
        Packet::TcpPacket packet;
        Packet::initPacket(packet);
        packet.header.dataLength =PIECE_SIZE;
        packet.header.packetType = Packet::DATA;
        packet.header.packetIndex = (uint32_t) i;
        /*leave the sequence number for lower level to decided*/

        readerPointer = headPointer + PIECE_SIZE*i;
        if (i+1==packetArray.size()){
            /*take care of the last pieces*/
            if (length%PIECE_SIZE>0) {
                /*last part of source data but can't fill the whole packet data field*/
                /*normal case*/
                packet.header.dataLength = length % PIECE_SIZE;
//                memcpy(&packet.data[0],readerPointer,packet.header.dataLength);
            } else{
                /*last part but coincidentally fully fill data field*/
//                memcpy(&packet.data[0],readerPointer,PIECE_SIZE);
            }
        } else{
            /*before the last part. this will fully fill the packet*/
//            memcpy(&packet.data[0],readerPointer,PIECE_SIZE);
        }

        memcpy(&packet.data[0],readerPointer,packet.header.dataLength);
        packetArray.at(i).first = packet;
        packetArray.at(i).second = false;
    }

    /*create new thread to receive ack*/
//    thread receiveAckThread(&Tcp::ackHandleThread,this);
    bool threadRun = true;
    boost::thread receiveAckThread(boost::bind(&Tcp::ackHandleThread, this,ref(packetArray),ref(threadRun)));

    vector<pair<Packet::TcpPacket,bool>>::iterator packetIt;

    bool firstSend = true;

    while (1) {
        packetIt = packetArray.begin();
        while (packetIt!=packetArray.end()){
            if (!packetIt->second) {

                if ((random() % 100) < 5) {
                    vector<pair<Packet::TcpPacket,bool>>::iterator  originalSend;
                    originalSend = packetIt;

                    /*jump to next unsent packet first*/
                    while (packetIt!=packetArray.end()){
                        packetIt++;
                        if (!packetIt->second){
                            sendPacketToBytes(packetIt->first);
                            logger.log(LOG_TYPE_INFO,"packet order changed manually ["
                                                     + to_string(originalSend->first.header.packetIndex)
                                                     + "->"
                                                     + to_string(packetIt->first.header.packetIndex)
                                                     + "]");
                            break;
                        }
                    }

                    /*go back to original packet waiting to send*/
                    packetIt = originalSend;

                }

                sendPacketToBytes(packetIt->first);

                if (!firstSend){
                    /*packet resent*/
                    logger.log(LOG_TYPE_INFO,"resend packet["+to_string(packetIt->first.header.packetIndex)+"]");
                }
            }
            packetIt++;
        }

        firstSend = false;

        /*wait ack*/
        usleep(100000);

        /*check if all ACKs received*/
        bool hasUnsentPacket = false;
        for (pair<Packet::TcpPacket,bool> pair : packetArray ){
            if (!pair.second){
                hasUnsentPacket = true;
                break;
            }
        }

        if (!hasUnsentPacket){

            /*close thread*/
            pthread_cancel(receiveAckThread.native_handle());


            Packet::TcpPacket ackPacket;
            Packet::initPacket(ackPacket);

            while (1){
                /*send PUSH packet*/
                Packet::TcpPacket pushPacket = Packet::getPushPacket();
                Tcp::sendPacketToBytes(pushPacket);

                /*wait for PUSH ACK*/
                receiveBytesToPacket(ackPacket);
                if (ackPacket.header.packetType == Packet::ACK){
                    break;
/*                    if (pushPacket.header.sequenceNumber == ackPacket.header.sequenceNumber+1){
                        break;
                    }*/
                }
                usleep(10000);
            }
            break;
        }

    }

    /*stop the ack receiving thread*/
//    receiveAckThread.~thread();
    receiveAckThread.~thread();

    logger.log(LOG_TYPE_DEBUG,"tcp write complete");

    return true;
}



bool Tcp::read(char *data, uint32_t &length) {
    rxBuffer.clear();

    if (tcpStatus == TCP_DISCONNECT){
        /*tcp connection broken*/
        logger.log(LOG_TYPE_FAIL,"unable to read, tcp not connected");
        return false;
    }

    /*received check array*/
    vector<bool> receivedPacketCheck;

    /*last data received timestamp*/
    time_t now = time(0);

    Packet::TcpPacket receivePacket;
    Packet::initPacket(receivePacket);

    /*total receive bytes*/
    uint32_t maxPacketIndex = 0;
    uint32_t maxPacketIndexDataLength=0;

    /*last ack received packet index */
    int lastAckIndex = 0;


    while (1){

        string peerIp;
        uint16_t peerPort;

        /*read data into packet*/
        int readyBytes = Tcp::receiveBytesToPacket(receivePacket,peerIp,peerPort);
        if (readyBytes<0){
            /*error*/
            continue;
        }

        /*TODO test drop packet later*/
        if (Tcp::opMode == SERVER_MODE) {
            if ((random() % 100) < 10) {
                /*drop the packet*/
                logger.log(LOG_TYPE_INFO,"packet drop [" + to_string(receivePacket.header.packetIndex) + "]");
                continue;
            }
        }

        if(lastAckIndex+1 != receivePacket.header.packetIndex){
            /*packer send order change detected*/
            logger.log(LOG_TYPE_INFO,"packet out of order ["+ to_string(lastAckIndex) + "->"+ to_string(receivePacket.header.packetIndex) +"] detected");
        }
        lastAckIndex = receivePacket.header.packetIndex;



        switch (receivePacket.header.packetType){
            case Packet::DATA:{
                /*save data*/
                /*extend buffer if needed*/
                uint32_t packetIndex =  receivePacket.header.packetIndex;
                uint32_t byteIndex = packetIndex << 10; /* times 1024*/
                if (receivedPacketCheck.size() <= packetIndex){
                    receivedPacketCheck.resize(packetIndex+8);
                    rxBuffer.resize(receivedPacketCheck.size()<<10 );
                }

                /*copy data*/
                memcpy(&rxBuffer[byteIndex],&receivePacket.data[0],receivePacket.header.dataLength);

                /*check if max index*/
                if (packetIndex>maxPacketIndex){
                    maxPacketIndex = packetIndex;
                    maxPacketIndexDataLength = receivePacket.header.dataLength;
                }

                /*mark received*/
                receivedPacketCheck[packetIndex] = true;

                /*write ack*/
                Packet::TcpPacket ackPacket = Packet::getAckPacket(receivePacket);
                sendPacketToBytes(ackPacket);


                break;
            }
            case Packet::FIN:{
                /*confirm with sender*/

                tcpStatus = TCP_DISCONNECT;
                break;
            }
            case Packet::ACK:{
//                return true;
                break;
            }
            case Packet::PUSH:{

                /*TODO check the if data is complete*/
                /*calculate total data length*/
                uint32_t totalBytesRcved;
                if(maxPacketIndexDataLength){
                    totalBytesRcved = (maxPacketIndex <<10) + maxPacketIndexDataLength;
                } else{
                    totalBytesRcved = (maxPacketIndex +1) <<10;
                }

                /*reply with ack*/
                Packet::TcpPacket pushAckPacket = Packet::getAckPacket(receivePacket);
                sendPacketToBytes(pushAckPacket);

                if (length<totalBytesRcved){
                    /*can't write all data to the given buffer*/
                    memcpy(data,&rxBuffer[0],length);
                    logger.log(LOG_TYPE_WARN,"buffer overflow");
                }else{
                    length = totalBytesRcved;
                    memcpy(data,&rxBuffer[0],totalBytesRcved);
                }

                rxBuffer.clear();


                return true;

                break;
            }
            default:{
                /*discard other packet*/
                logger.log(LOG_TYPE_WARN, "packet discarded due to undefined type");
                break;
            }
        }
    }

}

int Tcp::init(Mode mode) {
    socketFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socketFd < 0){
        logger.log(LOG_TYPE_FAIL, "socket create fail");
        return -1;
    }

    tcpStatus = TCP_DISCONNECT;

    switch (mode){
        case CLIENT_MODE:{
            /*socket is ready for use*/
            break;
        }
        case SERVER_MODE:{
            /*bind*/
            sockaddr_in my_addr;
            my_addr.sin_port = htons(Tcp::port);
            my_addr.sin_addr.s_addr = inet_addr(Tcp::ip.c_str());
            my_addr.sin_family = AF_INET;
            int status = bind(socketFd,(sockaddr*)&my_addr,sizeof(my_addr));
            if (status<0){
                logger.log(LOG_TYPE_FAIL, "[bind failed] " + string(strerror(errno)));
                return -1;
            }
            break;
        }
        default:{
            logger.log(LOG_TYPE_WARN, "mode not defined");
            break;
        }
    }


    return 0;
}


int Tcp::openSocket() {
    if (socketFd>0){
        /*socket already open*/
        logger.log(LOG_TYPE_FAIL, "socket already opened");
        return -1;
    }
    Tcp::init(Tcp::opMode);
    return 0;
}



int Tcp::closeSocket() {
    close(socketFd);
    socketFd = -1;
    return 0;
}

bool Tcp::connect() {
    if (Tcp::opMode == SERVER_MODE){
        /*fail*/
        logger.log(LOG_TYPE_FAIL,"can't connect to others in server mode");
        return false;
    }

    /*send sync packet to server*/
    Packet::TcpPacket syncPacket = Packet::getSyncPacket();
    sendPacketToBytes(syncPacket);


    string peerIp;
    uint16_t peerPort;

    /*wait for sync from server*/
    while (1){/*TODO wait for timeout!?*/
        Packet::initPacket(syncPacket);
        receiveBytesToPacket(syncPacket,peerIp,peerPort);

        if (syncPacket.header.packetType == Packet::SYNC){
            /*TODO check if peer ip and port match*/
            tcpStatus = TCP_CONNECT;

            /*connection established*/
            logger.log(LOG_TYPE_DEBUG,"tcp connection established");
            break;
        }
        usleep(1000);
    }


    return false;
}


bool Tcp::disconnect() {
    return false;
}





bool Tcp::accept() {
    if (Tcp::opMode!= SERVER_MODE){
        logger.log(LOG_TYPE_FAIL,"only server can accept connection");
        return false;
    }

    /*ONLY on server side*/

    Packet::TcpPacket packet;
    int bytesRecv = receiveBytesToPacket(packet, Tcp::peerIp, Tcp::peerPort);

    if (bytesRecv<0){
        return false;
    }

    if (packet.header.packetType == Packet::SYNC){

        if(tcpStatus == TCP_DISCONNECT){
            /*send sync back*/
            Packet::TcpPacket syncPacket = Packet::getSyncPacket();
            sendPacketToBytes(syncPacket);

            /*change status*/
            tcpStatus = TCP_CONNECT;

        } else if (tcpStatus == TCP_CONNECT){
            logger.log(LOG_TYPE_WARN,"only accept one client at a time, SYNC ignored");
            return false;
        }

        /*connection established*/
        logger.log(LOG_TYPE_DEBUG,"tcp connection established");
    }



    return true;

}

int Tcp::sendPacketToBytes(Packet::TcpPacket &packet) {

    vector<char> outBuffer;
    uint32_t realSize = sizeof(packet.header)+packet.header.dataLength;
    outBuffer.resize(realSize);

    /*copy packet to bytes buffer*/
    memcpy(&outBuffer[0],&packet,realSize);

    /*send bytes to peer*/
    sockaddr_in peer_addr;
    peer_addr.sin_port = htons(peerPort);
    peer_addr.sin_addr.s_addr = inet_addr(peerIp.c_str());
    peer_addr.sin_family = AF_INET;
    int  sentBytes =  sendto(socketFd, &outBuffer[0], realSize, 0, (struct sockaddr*)&peer_addr, sizeof(peer_addr));

    if (sentBytes<0){
        logger.log(LOG_TYPE_FAIL, "send fail with error: " + string(strerror(errno))) ;
        return -1;
    }

    return sentBytes;
}

int Tcp::receiveBytesToPacket(Packet::TcpPacket &packet) {
    /*discard ip and port info*/
    string ip;
    uint16_t port;
    Tcp::receiveBytesToPacket(packet,ip,port);
    return 0;
}


int Tcp::receiveBytesToPacket(Packet::TcpPacket &packet, string &peerAddress, uint16_t &peerPort) {
    vector<char> inBuffer;

    /*clean inBuffer*/
    inBuffer.resize(MAX_BUFFER_SIZE);

    /*clean packet*/
    Packet::initPacket(packet);

    /*socket setting*/
    sockaddr_in src_addr;
    socklen_t addrLength = sizeof(src_addr);

    /*packet container*/
    Packet::TcpPacket receivePacket;
    Packet::initPacket(receivePacket);

    /*read bytes*/
    uint32_t readBytes;
    readBytes = (uint32_t) recvfrom(Tcp::socketFd, &inBuffer[0], inBuffer.size(), 0, (sockaddr *)&src_addr, &addrLength);


    peerAddress = inet_ntoa(src_addr.sin_addr);
    peerPort = ntohs(src_addr.sin_port);


    if (readBytes>0){
        /*cast data to packet*/
        memcpy(&receivePacket, &inBuffer[0],readBytes);
    } else if (readBytes<0){
        /*receive error*/
        logger.log(LOG_TYPE_FAIL, "receive fail with error: " + string(strerror(errno))) ;
        return -1;
    } else{
        /*no byte read*/
    }


    Packet::TcpPacket bufPacket;
    Packet::initPacket(bufPacket);
    /*parse to packet*/
    memcpy(&bufPacket,&inBuffer[0],readBytes);

    /*TODO assume data are complete*/
    packet = bufPacket;
    return readBytes;
}

Tcp::TcpStatus Tcp::getStatus() {
    return Tcp::tcpStatus;
}

void Tcp::ackHandleThread(vector<pair<Packet::TcpPacket, bool>> &packetArray, bool &run) {

    Packet::TcpPacket ackPacket;
    Packet::initPacket(ackPacket);

    string senderIp;
    uint16_t senderPort;
    while (run) {
        Tcp::receiveBytesToPacket(ackPacket, senderIp, senderPort);

        if (ackPacket.header.packetType == Packet::ACK) {
            packetArray.at(ackPacket.header.packetIndex).second = true;


//            logger.log(LOG_TYPE_DEBUG,"ack received with sequence num: " + to_string(ackPacket.header.sequenceNumber)) ;
        } else {
            logger.log(LOG_TYPE_WARN, "receive non-ACK packet");
        }


//        usleep(1);
    }
    return;

}




























