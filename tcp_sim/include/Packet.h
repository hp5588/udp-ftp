//
// Created by brian on 2016/5/7.
//

#ifndef UDP_FTP_PACKET_H
#define UDP_FTP_PACKET_H

#include <stdint-gcc.h>
#include <cstring>
#define PIECE_SIZE 1024





class Packet{
public:
    enum PacketType{
        SYNC,
        ACK,
        DATA,
        PUSH,
        FIN
    };

    struct Header{
        /*header*/
        uint8_t packetType;
        uint32_t sequenceNumber;
        uint32_t packetIndex;
        uint16_t dataLength;

    };

    struct TcpPacket{
        /*header*/
        Header header;
        /*data*/
        char data[PIECE_SIZE];
        /*checksum!?*/
    };

    static int getHeaderLength(TcpPacket &packet){
        char *pHead = (char*)&packet.header;
        char *pDataHead = (char*)&packet.data;
    }

    static int getActualPacketLength (TcpPacket &packet){
        return sizeof(packet.header)+ packet.header.dataLength;
    };


    static void initPacket(TcpPacket &packet){
        memset(&packet,0,sizeof(packet));
    }

    static TcpPacket getSyncPacket(){
        TcpPacket synPacket;
        initPacket(synPacket);
        synPacket.header.packetType = SYNC;
        synPacket.header.sequenceNumber = 0;
        synPacket.header.dataLength = 0;

        return synPacket;
    }
    static TcpPacket getPushPacket(){
        TcpPacket pushPacket;
        initPacket(pushPacket);
        pushPacket.header.packetType = PUSH;
        pushPacket.header.sequenceNumber = 0;
        pushPacket.header.dataLength = 0;

        return pushPacket;
    }

    static TcpPacket getAckPacket(TcpPacket &receivedPacket){
        TcpPacket ackPacket;
        initPacket(ackPacket);
        ackPacket.header.packetType = ACK;
        ackPacket.header.sequenceNumber = receivedPacket.header.sequenceNumber+1;
        ackPacket.header.packetIndex = receivedPacket.header.packetIndex;
        ackPacket.header.dataLength = 0;

        return ackPacket;
    }

    static void printPacket(TcpPacket packet){
//        cout << "{" <<endl;
//        cout << setw(15) << right << packet.header.packetType ;
    }


};




#endif //UDP_FTP_PACKET_H
