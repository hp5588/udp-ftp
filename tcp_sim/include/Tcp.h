//
// Created by brian on 2016/5/7.
//

#ifndef UDP_FTP_TCP_H
#define UDP_FTP_TCP_H
#include <string>
#include <Logging.h>
#include <vector>
#include <list>
#include <Packet.h>

using namespace std;

#define MAX_BUFFER_SIZE 2000


class Tcp {


public:
    enum Mode{
        UNDEFINED,
        SERVER_MODE,
        CLIENT_MODE
    };

    enum TcpStatus{
        TCP_DISCONNECT,
        TCP_CONNECT
    };

    /*constructors*/
    Tcp(string ip, uint16_t port, Mode mode);

    /*disconnect*/
    int openSocket();
    int closeSocket();

    bool write(char* data, uint32_t length);
    bool read(char* data, uint32_t &length);
    bool connect();
    bool disconnect();
    bool accept();

    /*status*/
    Tcp::TcpStatus getStatus();

    bool isConnected(){
        if (Tcp::getStatus()==TCP_CONNECT){
            return true;
        }else if(Tcp::getStatus() == TCP_DISCONNECT){
            return false;
        }
    }


    /*verify*/
    bool isListener();
    bool isSender();




private:
    /*buffer*/
    vector<char> rxBuffer;
    vector<char> txBuffer;

    /*buffer status*/
    bool readyToRead = false;

    /*tcp status*/
    TcpStatus tcpStatus;


    /*low level sending*/
    int sendPacketToBytes(Packet::TcpPacket &packet);/*send default to the peer*/
    int receiveBytesToPacket(Packet::TcpPacket &packet, string &peerAddress, uint16_t &peerPort);
    int receiveBytesToPacket(Packet::TcpPacket &packet);

    /*tools*/
    Logging logger = Logging("TCP",true);

    Mode opMode = UNDEFINED;
    string ip;
    uint16_t port;

    string peerIp;
    uint16_t peerPort;

    int socketFd = -1;

    /*thread callback*/
    void ackHandleThread(vector<pair<Packet::TcpPacket, bool>> &packetArray, bool &run);

    /*method*/
    int init(Mode mode);





};


#endif //UDP_FTP_TCP_H
