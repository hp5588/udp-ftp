cmake_minimum_required(VERSION 3.0)
project(tcp)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
find_package(Threads REQUIRED)
find_package(
        Boost
        COMPONENTS thread
        REQUIRED)

set(TCP_INCLUDE_DIRS ${PROJECT_SOURCE_DIR}/include PARENT_SCOPE)
#include_directories(${PROJECT_SOURCE_DIR}/include)
include_directories(include)
include_directories(${Boost_INCLUDE_DIRS})
include_directories(${TOOLS_INCLUDE_DIRS})

set(SOURCE_FILES
        include/Tcp.h src/Tcp.cpp )

add_library(${PROJECT_NAME} STATIC ${SOURCE_FILES})
target_link_libraries(${PROJECT_NAME} tools ${Boost_LIBRARIES} pthread)
